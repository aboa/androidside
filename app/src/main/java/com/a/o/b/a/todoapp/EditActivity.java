package com.a.o.b.a.todoapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;


public class EditActivity extends AppCompatActivity {

    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    public String dueDate;

    private TextView mDisplayTime;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;
    public String dueTime;

    public int id;
    EditText edTitle;
    EditText edDescription;
    Button buttonEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        edTitle = (EditText) findViewById(R.id.editTitle);
        edDescription = (EditText) findViewById(R.id.editDescription);
        buttonEdit = (Button) findViewById(R.id.editButton);
        mDisplayDate = (TextView) findViewById(R.id.tvDate);
        mDisplayTime = (TextView) findViewById(R.id.tvTime);

        id = getIntent().getIntExtra("id", 0);
        edTitle.setText(getIntent().getStringExtra("title"));
        edDescription.setText(getIntent().getStringExtra("description"));
        mDisplayDate.setText(getIntent().getStringExtra("date"));
        dueDate = getIntent().getStringExtra("date");


        mDisplayTime.setText(getIntent().getStringExtra("time"));
        dueTime = getIntent().getStringExtra("time");


        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int year = 0;
                int month = 0;
                int day = 0;
                if (!dueDate.equals("null")) {
                    year = Integer.valueOf(dueDate.substring(0, 4));
                    month = Integer.valueOf(dueDate.substring(5, 7));
                    day = Integer.valueOf(dueDate.substring(8));
                }

                DatePickerDialog dialog = new DatePickerDialog(
                        com.a.o.b.a.todoapp.EditActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDisplayTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hour = 0;
                int minute = 0;
                if (!dueTime.equals("null")) {
                    hour = Integer.valueOf(dueTime.substring(0, 2));
                    minute = Integer.valueOf(dueTime.substring(3, 5));
                }
                TimePickerDialog dialog = new TimePickerDialog(
                        EditActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mTimeSetListener,
                        hour, minute, true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d("", "onDateSet: mm/dd/yyyy: " + day + "/" + month + "/" + year);

                String date = day + "/" + month + "/" + year;
                mDisplayDate.setText(date);
                dueDate = date;
            }
        };

        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                Log.e("TIME", "hh:mm" + hourOfDay + ":" + minute);

                String time = hourOfDay + ":" + minute;

                mDisplayTime.setText(time);
                dueTime = time;

            }
        };
    }


    public void editedMessageWarn() {
        Context context = getApplicationContext();
        CharSequence text = "Note edited!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

    }

    public void backtoMainActivity() {

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    public void editTask(View view) {

        String title = edTitle.getText().toString();
        String desc = edDescription.getText().toString();

        Log.e("Edit", id + " " + title + " " + desc + " " + dueDate);

        new UpdateRequest().HttpUpdate(id, title, desc, dueDate, dueTime);

        editedMessageWarn();

        backtoMainActivity();


    }

}



