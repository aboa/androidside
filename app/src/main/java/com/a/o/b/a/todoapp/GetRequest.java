package com.a.o.b.a.todoapp;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Calendar;

import javax.net.ssl.HttpsURLConnection;


public class GetRequest extends AsyncTask {

    public String[][] tasks;

    public String[] title;

    public int[] id;

    @Override
    protected Object doInBackground(Object[] params) {

        httpGet();


        return null;
    }


    public String[][] httpGet() {

        URL url = null;

        try {
            url = new URL("https://aboa.herokuapp.com/todo_list_webs.json");

            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            con.connect();

            BufferedReader bf = new BufferedReader(new InputStreamReader(con.getInputStream()));

            StringBuffer buff = new StringBuffer();

            String line = "";

            while ((line = bf.readLine()) != null) {
                buff.append(line);
                //buff.append("\n");
            }

            bf.close();
            Log.e("MESAJ", buff.toString());

            String finalJSON = buff.toString();

            JSONArray parentArray = new JSONArray(finalJSON);


            tasks = new String[parentArray.length()][5];

            int j = 0;
            for (int i = 0; i < parentArray.length(); i++) {

                JSONObject finalObject = parentArray.getJSONObject(i);

                int id = finalObject.getInt("id");
                String title = finalObject.getString("title");
                String description = finalObject.getString("description");
                String dateTime = finalObject.getString("date");
                String date = "null";
                String time = "null";
                if (!dateTime.equals("null")) {
                    date = dateTime.substring(0, 10);
                    time = dateTime.substring(11, 16);
                }

                Calendar cal = Calendar.getInstance();

                tasks[j][0] = "" + id;
                tasks[j][1] = title;
                tasks[j][2] = description;
                tasks[j][3] = date;
                tasks[j++][4] = time;


            }

            Log.e("STATUS", String.valueOf(con.getResponseCode()));
            Log.e("MSG", con.getResponseMessage());

            id = new int[tasks.length];
            for (int i = 0; i < tasks.length; i++) {
                id[i] = Integer.valueOf(tasks[i][0]);
            }

            title = new String[tasks.length];
            for (int i = 0; i < tasks.length; i++) {
                title[i] = tasks[i][1];
                //Log.e("getTitle ARR", title[i]);

            }

            con.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return tasks;
    }

    public int[] getIds() {

        execute();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return this.id;

    }

    public String[] getTitle() {

        execute();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return this.title;
    }


    public String[][] getTasks() {

        execute();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return tasks;
    }


}


