package com.a.o.b.a.todoapp;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.*;


public class MainActivity extends AppCompatActivity {

    ArrayAdapter<String> titleAdapter;
    ListView lstTask;
    int[] idArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!isOnline()) {

            Toast.makeText(getApplicationContext(), "No network connection.", Toast.LENGTH_LONG).show();
            MainActivity.this.finish();

        }

        lstTask = (ListView) findViewById(R.id.list_view);
        try {
            loadTaskList();
        } catch (NullPointerException e) {
            Toast.makeText(getApplicationContext(), "Cannot access to website.", Toast.LENGTH_LONG).show();
            finish();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addingMessage();
            }
        });

        /*
        boolean alarm = (PendingIntent.getBroadcast(this, 0, new Intent("ALARM"), PendingIntent.FLAG_NO_CREATE) == null);

        if(alarm){
            Intent itAlarm = new Intent("ALARM");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,itAlarm,0);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.add(Calendar.SECOND, 3);
            AlarmManager alarme = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarme.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),60000, pendingIntent);
        }
        */
    }

    protected void loadTaskList() throws NullPointerException {

        ArrayList<String> titleList = new ArrayList<>();
        ArrayList<String> idtitleList = new ArrayList<>();

        String task[][] = new GetRequest().getTasks();

        String[] titleArray = new String[task.length];
        for (int i = 0; i < task.length; i++) {
            titleArray[i] = task[i][1];
        }
        idArray = new int[task.length];
        for (int i = 0; i < task.length; i++) {
            idArray[i] = Integer.valueOf(task[i][0]);
        }
        //String[] titleArray = new GetRequest().getTitle();
        //idArray = new GetRequest().getIds();
        String[] id_title = idWithTitle(idArray, titleArray);

        for (int i = 0; i < titleArray.length; i++) {
            idtitleList.add(id_title[i]);
        }


        if (titleAdapter == null) {
            titleAdapter = new ArrayAdapter<>(this, R.layout.row, R.id.task_title, idtitleList);
            lstTask.setAdapter(titleAdapter);

        } else {

            titleAdapter.clear();
            titleAdapter.addAll(titleList);
            titleAdapter.notifyDataSetChanged();

        }


    }

    public void showTask(View view) {

        TextView listText = (TextView) view.findViewById(R.id.task_title);
        String text = listText.getText().toString();
        int punc = text.indexOf(":");
        text = text.substring(0, punc - 1);
        int id = Integer.valueOf(text);

        Log.e("texttttttttt", text);
        int value = 0;
        Intent viewIntent = new Intent(MainActivity.this, ViewActivity.class);
        for (int i = 0; i < idArray.length; i++) {
            if (id == idArray[i]) {
                value = i;
            }
        }

        String task[][] = new GetRequest().getTasks();
        int taskID = Integer.valueOf(task[value][0]);
        String title = task[value][1];
        String desc = task[value][2];
        String date = task[value][3];
        String time = task[value][4];


        viewIntent.putExtra("id", taskID);
        viewIntent.putExtra("description", desc);
        viewIntent.putExtra("title", title);
        viewIntent.putExtra("date", date);
        viewIntent.putExtra("time", time);


        startActivity(viewIntent);

    }

    public void addingMessage() {

        Intent intent = new Intent(this, AddingMessageActivity.class);
        startActivity(intent);
    }


    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public String[] idWithTitle(int[] ids, String[] titles) {

        String[] id_title = new String[ids.length];

        for (int i = 0; i < ids.length; i++) {
            id_title[i] = ids[i] + " : " + titles[i];

        }
        return id_title;

    }
}
