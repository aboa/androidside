package com.a.o.b.a.todoapp;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

public class ViewActivity extends AppCompatActivity {

    int id;
    String desc;
    String title;
    String date;
    String time;
    NotificationCompat.Builder notification;
    private static final int uniqueID = 333;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

        id = getIntent().getIntExtra("id", 0);

        desc = getIntent().getStringExtra("description");
        TextView textDesc = (TextView) findViewById(R.id.tvDesc);
        textDesc.setText(desc);

        title = getIntent().getStringExtra("title");
        TextView textTitle = (TextView) findViewById(R.id.tvTitle);
        textTitle.setText(title);

        date = getIntent().getStringExtra("date");
        TextView textDate = (TextView) findViewById(R.id.tvDateEdit);
        textDate.setText(date);

        time = getIntent().getStringExtra("time");
        TextView textTime = (TextView) findViewById(R.id.tvTime);
        textTime.setText(time);

        Button deleteButton = (Button) findViewById(R.id.delete_task);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteTask(view,title,desc);

            }
        });

        Button editButton = (Button) findViewById(R.id.edit_task);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editTask();
            }
        });

    }

    protected void editTask() {

        Intent editIntent = new Intent(this, EditActivity.class);
        editIntent.putExtra("id", id);
        editIntent.putExtra("description", desc);
        editIntent.putExtra("title", title);
        editIntent.putExtra("date", date);
        editIntent.putExtra("time", time);
        startActivity(editIntent);

    }

    protected void deleteTask(View view,String title,String desc) {

        new DeleteRequest().httpDelete(id);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        deleteButtonClicked(view,title,desc);

    }

    public void deleteButtonClicked(View view,String title,String desc){

        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("A TASK DELETED");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("Title: " + "' " + title + " '" + " deleted !");
        notification.setContentText(desc);

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }


}
