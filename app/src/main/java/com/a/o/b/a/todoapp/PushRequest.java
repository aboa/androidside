package com.a.o.b.a.todoapp;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class PushRequest extends AsyncTask {

    String title;
    String description;
    String date;

    @Override
    protected Object doInBackground(Object[] params) {

        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("https://aboa.herokuapp.com/todo_list_webs");

            String json = "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("title", title);
            jsonObject.accumulate("description", description);
            jsonObject.accumulate("date", date);

            json = jsonObject.toString();

            StringEntity se = new StringEntity(json);
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);

        } catch (JSONException e) {
            Log.i("JSONEx", e.toString());
        } catch (UnsupportedEncodingException e) {
            Log.i("UnsupEncodingEx", e.toString());
        } catch (IOException e) {
            Log.i("IOEx", e.toString());

        }

        return null;
    }

    public void httpPost(String tit, String desc, String dueDate, String dueTime) {

        this.title = tit;
        this.description = desc;
        this.date = dueDate + "T" + dueTime + ":00.000Z";

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.execute();

    }

}
