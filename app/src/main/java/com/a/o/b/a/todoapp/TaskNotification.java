package com.a.o.b.a.todoapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

public class TaskNotification extends Service {

    NotificationCompat.Builder notification;
    private static final int uniqueID = 999;

    public TaskNotification() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate()
    {

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

        sendNotification();

    }

    public void sendNotification(){


        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("Deneme");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("title for trying");
        notification.setContentText("text for trying");

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());


    }
}
