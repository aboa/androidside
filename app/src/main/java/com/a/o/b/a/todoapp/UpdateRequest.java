package com.a.o.b.a.todoapp;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class UpdateRequest extends AsyncTask {

    public int id;

    public String body;

    public String updatedItem;

    @Override
    protected Object doInBackground(Object[] params) {
        Log.e("post", "" + id);
        HttpURLConnection connection = null;

        try {
            URL url = new URL("https://aboa.herokuapp.com/todo_list_webs/" + id);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Accept-Charset", "utf-8,*");
            OutputStreamWriter streamWriter = new OutputStreamWriter(connection.getOutputStream());

            streamWriter.write(body);
            streamWriter.flush();
            StringBuilder stringBuilder = new StringBuilder();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(streamReader);
                String response = null;
                while ((response = bufferedReader.readLine()) != null) {
                    stringBuilder.append(response + "\n");
                }
                bufferedReader.close();
                updatedItem = stringBuilder.toString();

                Log.e("Post-Response", stringBuilder.toString());
            } else {
                Log.e("Post-Error", connection.getResponseMessage());
            }
        } catch (Exception exception) {
            Log.e("Post-Exception", exception.toString());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;

    }

    public String HttpUpdate(int id, String title, String description, String dueDate , String dueTime) {

        JSONObject request = new JSONObject();

        this.id = id;

        try {

            request.putOpt("title", title);
            request.putOpt("description", description);
            request.putOpt("date", dueDate + "T" + dueTime +":00.000Z");

            body = request.toString();
            this.execute();

            return updatedItem;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
